//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Winter
{
    public class GameSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Player player;
        public Survivor[] survivors;
        public Terrain terrain;
        
    }
}
