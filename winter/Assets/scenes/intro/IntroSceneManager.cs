//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Winter
{
    public class IntroSceneManager
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Camera cam;
        public UIElement fader;

        public Animator plane;

        public GameObject engineFire;

        public AudioSource planeSfx;
        public AudioSource alarmSfx;
        public AudioSource planeSlowDownSfx;
        public AudioSource horrorSfx;
        public AudioSource crashSfx;
        
        public Transform cameraAngle0;
        public Transform cameraAngle1;
        public Transform cameraAngle2;

        public Transform cameraAngle3;
        public Transform cameraAngle4;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Start()
        {
            StartCoroutine(PlayCutscene());
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator PlayCutscene()
        {
            yield return fader.ShowAsync(0f);
    
            engineFire.SetActive(false);
            cam.transform.Copy(cameraAngle0);
            planeSfx.Play();

            yield return fader.HideAsync(1f);

            yield return cam.transform.MoveTo(cameraAngle1, 5f);
            yield return cam.transform.MoveTo(cameraAngle2, 2f);
            
            yield return new WaitForSeconds(2f);

            engineFire.SetActive(true);
            alarmSfx.Play();

            yield return new WaitForSeconds(5f);

            horrorSfx.Play();
            planeSlowDownSfx.Play();
            cam.transform.Copy(cameraAngle3);
            yield return cam.transform.MoveTo(cameraAngle4, 4f);

            yield return fader.ShowAsync(1f);

            planeSlowDownSfx.Stop();
            crashSfx.Play();
            alarmSfx.Stop();
            planeSfx.Stop();


            yield return new WaitForSeconds(8f);
            GameManager.ChangeScene(SceneId.GameScene);

        }
    }
}
