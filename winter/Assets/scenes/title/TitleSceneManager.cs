//
// (c) GameJammers 2021
// http://www.jamming.games
//

using UnityEngine;

namespace Winter
{
    public class TitleSceneManager
        : MonoBehaviour
    {
        public void StartGame()
        {
            GameManager.ChangeScene(SceneId.IntroScene);
        }
    }
}
