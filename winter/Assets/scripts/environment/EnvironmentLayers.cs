//
// (c) GameJammers 2019
// http://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Winter
{
	public static class EnvironmentLayers
	{
		// contants ///////////////////////////////////////////////////////////
        public static readonly string kDefaultLayerName         = "Default";
        public static readonly string kIgnoreRaycastLayerName   = "Ignore Raycast";
		public static readonly string kAgentLayerName			= "Agent";
		public static readonly string kWalkableLayerName		= "Walkable";
        public static readonly string kObstacleLayerName        = "Obstacle";
		public static readonly string kIgnoreNavigationName		= "Ignore Navigation";

        public static readonly LayerMask Default                = LayerMask.GetMask( kDefaultLayerName );
        public static readonly LayerMask kIgnoreRaycast         = LayerMask.GetMask( kIgnoreRaycastLayerName );
		public static readonly LayerMask Agent					= LayerMask.GetMask( kAgentLayerName );
		public static readonly LayerMask Walkable				= LayerMask.GetMask( kWalkableLayerName );
        public static readonly LayerMask Obstacle               = LayerMask.GetMask( kObstacleLayerName );
		public static readonly LayerMask IgnoreNavigation		= LayerMask.GetMask( kIgnoreNavigationName );
	}

	public static class EnvironmentTags
	{
        public static readonly string Player                    = "Player";
        public static readonly string Environment               = "Environment";
		public static readonly string RoomFootprint				= "RoomFootprint";
    }
}
