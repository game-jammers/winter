//
// (c) GameJammers 2021
// http://www.jamming.games
//

namespace Winter
{
    public interface IDisplay
    {
        string displayName                                      { get; }
        string description                                      { get; }
    }
}
