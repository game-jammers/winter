//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using System.Collections;

namespace Winter
{
    public class Pickup
        : MonoBehaviour
        , IDisplay
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public string displayName                               { get { return $"{m_displayName} (F) To Pickup"; } }
        public string description                               { get { return $"Food ({food}) . Warmth ({warmth}) . Heal ({heal}) . Calm ({calm})"; } }
        [SerializeField] string m_displayName;
        public LayerMask dropMask;

        public GameSceneManager scene                           { get { return BaseSceneManager.GetInstance<GameSceneManager>(); } }
        public bool respawnOnStart                              = true;

        public int food;
        public int warmth;
        public int heal;
        public int calm;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Respawn()
        {
            transform.position = Vector3Extension.Random(-250f, 250f, 100f, 100f, -250f, 250f);
            Drop();
        }

        //
        // --------------------------------------------------------------------
        //
        

        public void Drop()
        {
            transform.position= new Vector3(
                transform.position.x,
                scene.terrain.SampleHeight(transform.position),
                transform.position.z
            );
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RespawnTimer()
        {
            while(true)
            {
                yield return new WaitForSeconds(120f);
                if((scene.player.transform.position - transform.position).magnitude > 10f)
                {
                    if(scene.player.heldItem != this)
                    {
                        Respawn();
                    }
                }
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if(respawnOnStart)
            {
                Respawn();
            } 

            StartCoroutine(RespawnTimer());
        }
    }
}
