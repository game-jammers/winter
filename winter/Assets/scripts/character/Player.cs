//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Winter
{
    [RequireComponent(typeof(CharacterController))]
    public class Player
        : MonoBehaviour
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Action
        {
            MoveX,
            MoveY,
            LookX,
            LookY,
            Interact
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Controls")]
        public Transform cameraMount                            = null;
        public Transform itemMount                              = null;
        public float lookSpeed                                  = 10f;
        public float moveSpeed                                  = 10f;

        public btCamera cam                                     { get; private set; }
        public bool hasCamera                                   { get { return cam != null; } }

        private CharacterController control                     = null;
        private InputRouter<Action> input                       = null;

        [ReadOnly] public Vector3 lookDir                       = Vector3.forward;
        [ReadOnly] public Pickup heldItem                       = null;

        [Header("Interface")]
        public Image reticle;
        public Color reticleNeutral;
        public Color reticleHighlight;

        public TextMeshProUGUI displayName;
        public TextMeshProUGUI displayDesc;

        

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void OnTakeCamera(btCamera camera)
        {
            cam = camera;
            cam.transform.SetParent(cameraMount);
            cam.transform.localPosition = Vector3.zero;
            cam.transform.localRotation = Quaternion.identity;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OnReleaseCamera()
        {
            cam.transform.SetParent(null);
            cam = null;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Pickup(Pickup pickup)
        {
            if(heldItem != null)
            {
                heldItem.transform.SetParent(null);
                heldItem.transform.position += Vector3.up * 100f;
                heldItem.Drop();
            }

            heldItem = pickup;
            pickup.transform.SetParent(itemMount);
            pickup.transform.localPosition = Vector3.zero;
            pickup.transform.localRotation = Quaternion.identity;
        }

        //
        // --------------------------------------------------------------------
        //

        private void GiveItem(Survivor survivor)
        {
            if(heldItem != null && survivor.isAlive)
            {
                survivor.hunger = System.Math.Max(0, survivor.hunger - heldItem.food);
                survivor.cold = System.Math.Max(0, survivor.cold - heldItem.warmth);
                survivor.pain = System.Math.Max(0, survivor.pain - heldItem.heal);
                survivor.fear = System.Math.Max(0, survivor.fear - heldItem.calm);
                survivor.ThankYou();

                heldItem.transform.SetParent(null);
                heldItem.Respawn();
                heldItem = null;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            control = GetComponent<CharacterController>();
            input = new InputRouter<Action>();

            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(Action.LookX, InputAction.FromAxis(InputAction.Axis.MouseHorizontal, false, 1f));
            input.Bind(Action.LookY, InputAction.FromAxis(InputAction.Axis.MouseVertical, true, 1f));
            input.Bind(Action.Interact, InputAction.FromKey(KeyCode.F));

            RaycastHit hit = default(RaycastHit);
            if(Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                transform.position = hit.point + Vector3.up;
            }

            BaseSceneManager.instance.RequestCamera(this);
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Update()
        {
            if(!hasCamera) return;

            BaseSceneManager.instance.CaptureCursor();

            //
            // look input
            //
            Vector3 deltaLook = new Vector3(
                input.GetAxis(Action.LookY) * lookSpeed,
                input.GetAxis(Action.LookX) * lookSpeed,
                0f
            );

            lookDir += deltaLook * Time.deltaTime;
            lookDir.x = btMath.Clamp(lookDir.x, -90f, 90f);
            lookDir.y = lookDir.y % 360f;
            cameraMount.transform.rotation = Quaternion.Euler(lookDir.x, lookDir.y, 0f);

            //
            // gravity
            //
            Vector3 delta = Physics.gravity;

            //
            // move input
            //
            Vector3 moveDelta = new Vector3(
                input.GetAxis(Action.MoveX) * moveSpeed,
                0f,
                input.GetAxis(Action.MoveY) * moveSpeed
            );

            delta += cam.TransformDirection(moveDelta);

            control.Move(delta * Time.deltaTime);

            //
            // pick for items
            //
            reticle.color = reticleNeutral;
            displayName.text = System.String.Empty;
            displayDesc.text = System.String.Empty;

            RaycastHit hit = default(RaycastHit);
            if(cam.MousePick(out hit))
            {
                IDisplay display = hit.collider.GetComponent<IDisplay>();

                if(display != null)
                {
                    reticle.color = reticleHighlight;
                    displayName.text = display.displayName;
                    displayDesc.text = display.description;
                }

                if(input.GetKeyPressed(Action.Interact))
                {
                    Pickup pickup = hit.collider.GetComponent<Pickup>();
                    Survivor survivor = hit.collider.GetComponent<Survivor>();
                    if(pickup != null)
                    {
                        Pickup(pickup);
                    }
                    else if(survivor != null)
                    {
                        GiveItem(survivor);
                    }
                }
            }
        }
    }
}
