//
// (c) GameJammers 2021
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;

//
// ############################################################################
//

namespace Winter
{
    public class Survivor
        : MonoBehaviour
        , IDisplay
    {

        //
        // constants //////////////////////////////////////////////////////////
        //

        static readonly string[] kHungerString = new string[] {
            "Im so hungry",
            "I need to eat",
            "My stomach is empty",
            "I am weak with hunger"
        };
        
        //
        // ----------------------------------------------------------------------------
        //
        
        static readonly string[] kColdString = new string[] {
            "Its so cold",
            "I cant feel my hands",
            "Its freezing",
        };
        
        //
        // ----------------------------------------------------------------------------
        //
        
        static readonly string[] kPainString = new string[] {
            "Everything hurts",
            "I think something is broken",
            "So much pain",
            "Ouch"
        };
        
        //
        // ----------------------------------------------------------------------------
        //
        
        static readonly string[] kFearString = new string[] {
            "Im so scared",
            "How can we survive",
            "Do you hear wolves?",
            "Are we going to die?"
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int seed;
        public bool isAlive                                     = true;

        public int hunger                                       = 0;
        public int cold                                         = 0;
        public int pain                                         = 0;
        public int fear                                         = 0;

        public float tick                                       = 10f;
        public float hungerMulti                                = 1f;
        public float coldMulti                                  = 1f;
        public float painMulti                                  = 1f;
        public float fearMulti                                  = 1f;

        public Animator animator;
        public TextMeshPro statusText                           = null;

        private System.Random rng;

        public string displayName                               { get { return isAlive ? $"{m_displayName} - [F] Give Item" : $"{m_displayName} [DEAD]"; } }
        public string description                               { get { return isAlive ? $"Hunger ({hunger}) . Cold ({cold}) . Pain ({pain}) . Fear ({fear})" : System.String.Empty; } }
        [SerializeField] string m_displayName                   = "Survivor";

        //
        // publi methods //////////////////////////////////////////////////////
        //

        public void ThankYou()
        {
            statusText.text = "Thank You, Doc";
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator UpdateStats()
        {
            while(isAlive)
            {
                yield return new WaitForSeconds(rng.Range(tick*0.75f, tick*1.25f));

                if(hunger + cold + pain + fear > 100)
                {
                    Death();
                }
                else
                {
                    switch(rng.Range(0,3))
                    {
                        case 0: hunger += (int)Mathf.Round(rng.Range(0,5) * hungerMulti); break;
                        case 1: cold += (int)Mathf.Round(rng.Range(0,5) * coldMulti); break;
                        case 2: pain += (int)Mathf.Round(rng.Range(0,5) * painMulti); break;
                        case 3: fear += (int)Mathf.Round(rng.Range(0,5) * fearMulti); break;
                    }
                }

                RefreshStatusText();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Death()
        {
            isAlive = false;
            animator.SetTrigger("death");
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void RefreshStatusText()
        {
            string result = kHungerString.Random(rng);
            int max = hunger;
            if(cold > max)
            {
                max = cold;
                result = kColdString.Random(rng);
            }

            if(pain > max)
            {
                max = pain;
                result = kPainString.Random(rng);
            }

            if(fear > max)
            {
                max = fear;
                result = kFearString.Random(rng);
            }

             statusText.text = result;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if(seed == 0) seed = btRandom.RandomInt();
            rng = new System.Random(seed);
            hunger = rng.Range(1,10);
            cold = rng.Range(1,10);
            pain = rng.Range(1,10);
            fear = rng.Range(1,10);
            RefreshStatusText();
            StartCoroutine(UpdateStats());
        }
    }
}
