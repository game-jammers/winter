//
// (c) GameJammers 2019
// http://jamming.games/
//

using blacktriangles;
using UnityEngine;

namespace Winter
{
    public enum SceneId
    {
        TitleScene = 0,
        IntroScene,
        GameScene,
    }
}
