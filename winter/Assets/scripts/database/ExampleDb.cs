//
// (c) GameJammers 2019
// http://jamming.games/
//

using blacktriangles;
using UnityEditor;

namespace Winter
{
    public class ExampleDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        [ShowInInspector] public GameManager[] managers         { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
           managers = LoadAll<GameManager>("managers"); 
        }
    }
}
